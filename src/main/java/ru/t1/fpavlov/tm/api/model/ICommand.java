package ru.t1.fpavlov.tm.api.model;

import org.jetbrains.annotations.Nullable;

/**
 * Created by fpavlov on 07.12.2021.
 */
public interface ICommand {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    void execute();

}
