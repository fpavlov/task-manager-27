package ru.t1.fpavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.Project;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    Project create(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

}
