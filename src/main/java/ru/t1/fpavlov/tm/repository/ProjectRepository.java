package ru.t1.fpavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.model.Project;

/*
 * Created by fpavlov on 10.10.2021.
 */
public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project item = new Project(name);
        item.setUserId(userId);
        return this.add(item);
    }

    @Nullable
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Project item = new Project(name, description);
        item.setUserId(userId);
        return this.add(item);
    }

}
