package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Clear task repository";

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = this.getUserId();
        this.getTaskService().clear(userId);
    }

}
