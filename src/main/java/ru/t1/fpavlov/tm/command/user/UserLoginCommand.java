package ru.t1.fpavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Login";

    @NotNull
    public static final String NAME = "login";

    @NotNull
    @Override
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String login = this.input("Login:");
        @NotNull final String password = this.input("Password:");
        this.getAuthService().login(login, password);
    }

}
