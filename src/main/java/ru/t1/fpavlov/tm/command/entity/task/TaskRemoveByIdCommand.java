package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.Task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove Task by id";

    @NotNull
    public static final String NAME = "task-remove-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Task entity = this.findById();
        @NotNull final String userId = this.getUserId();
        this.getTaskService().remove(userId, entity);
    }

}
