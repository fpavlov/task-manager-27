package ru.t1.fpavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.service.IAuthService;
import ru.t1.fpavlov.tm.api.service.IUserService;
import ru.t1.fpavlov.tm.command.AbstractCommand;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;
import ru.t1.fpavlov.tm.util.TerminalUtil;

/**
 * Created by fpavlov on 21.12.2021.
 */
public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    protected static final String ARGUMENT = null;

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    protected IAuthService getAuthService() {
        return this.getServiceLocator().getAuthService();
    }

    @NotNull
    public IUserService getUserService() {
        return this.getServiceLocator().getUserService();
    }

    @NotNull
    public String input(@Nullable final String displayText) {
        if (displayText != null && !displayText.isEmpty()) System.out.println(displayText);
        return TerminalUtil.nextLine();
    }

    public void renderUser(@Nullable final User user) {
        if (user != null) System.out.println(user);
    }

}
