package ru.t1.fpavlov.tm.command.entity.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectFindByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Find project by id and then display it";

    @NotNull
    public static final String NAME = "project-find-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Project entity = this.findById();
        System.out.println(entity);
    }

}
