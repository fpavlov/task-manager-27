package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Create new Task";

    @NotNull
    public static final String NAME = "task-create";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String name = this.askEntityName();
        @NotNull final String description = this.askEntityDescription();
        @NotNull final String userId = this.getUserId();
        this.getTaskService().create(userId, name, description);
    }

}
