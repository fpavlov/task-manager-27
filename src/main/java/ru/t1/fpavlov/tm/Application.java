package ru.t1.fpavlov.tm;

import ru.t1.fpavlov.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();

        bootstrap.run(args);
    }

}
