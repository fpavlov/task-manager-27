package ru.t1.fpavlov.tm.util;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 28.09.2021.
 */
public interface FormatUtil {

    @NotNull
    static String bytesToHumanReadable(long bytes) {
        @NotNull String convertedBytes;

        long kiloBytes = 1024;
        long megaBytes = kiloBytes * 1024;
        long gigaBytes = megaBytes * 1024;
        long teraBytes = gigaBytes * 1024;

        if (bytes >= kiloBytes && bytes < megaBytes) {
            convertedBytes = bytes / kiloBytes + " KB";
        } else if (bytes >= megaBytes && bytes < gigaBytes) {
            convertedBytes = bytes / megaBytes + " MB";
        } else if (bytes >= gigaBytes && bytes < teraBytes) {
            convertedBytes = bytes / gigaBytes + " GB";
        } else if (bytes >= teraBytes) {
            convertedBytes = bytes / teraBytes + " TB";
        } else {
            convertedBytes = bytes + " Bytes";
        }

        return convertedBytes;
    }

}
