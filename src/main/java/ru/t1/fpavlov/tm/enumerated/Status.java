package ru.t1.fpavlov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by fpavlov on 25.11.2021.
 */
public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    @NotNull Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return this.displayName;
    }

    @NotNull
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(@Nullable final String displayValue) {
        if (displayValue == null || displayValue.isEmpty()) return null;
        for (final Status item : values()) {
            if (item.displayName.equals(displayValue)) return item;
        }
        return null;
    }

    @NotNull
    public static String[] displayValues() {
        @NotNull final Status statusNames[] = Status.values();
        @NotNull final String statusValues[] = new String[statusNames.length];
        for (int i = 0; i < statusValues.length; i++) {
            statusValues[i] = statusNames[i].getDisplayName();
        }
        return statusValues;
    }

}
