package ru.t1.fpavlov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 19.12.2021.
 */
public enum Role {

    USER("Common user"),
    ADMIB("System admin");

    @NotNull
    private final String displayName;

    @NotNull Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return this.displayName;
    }

}
