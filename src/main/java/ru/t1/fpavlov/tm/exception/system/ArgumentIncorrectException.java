package ru.t1.fpavlov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class ArgumentIncorrectException extends AbstractSystemException {

    @NotNull
    public ArgumentIncorrectException() {
        super("Error! Incorrect argument");
    }

    @NotNull
    public ArgumentIncorrectException(@Nullable final String argument) {
        super("Error! Argument " + argument + "is incorrect.");
    }

}
