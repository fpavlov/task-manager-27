package ru.t1.fpavlov.tm.exception.user;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 24.01.2022.
 */
public final class AuthenticationException extends AbstractUserException {

    @NotNull
    public AuthenticationException() {
        super("Error! Authentication is fallen, your account is lock");
    }

}
