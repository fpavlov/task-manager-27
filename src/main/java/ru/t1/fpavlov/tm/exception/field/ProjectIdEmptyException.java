package ru.t1.fpavlov.tm.exception.field;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class ProjectIdEmptyException extends AbstractFieldException {

    @NotNull
    public ProjectIdEmptyException() {
        super("Error! Project id is empty");
    }

}
