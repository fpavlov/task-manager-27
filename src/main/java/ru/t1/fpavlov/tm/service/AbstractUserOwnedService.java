package ru.t1.fpavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.IUserOwnedRepository;
import ru.t1.fpavlov.tm.api.service.IUserOwnedService;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.IndexIncorrectException;
import ru.t1.fpavlov.tm.exception.user.UserIdEmptyException;
import ru.t1.fpavlov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 14.01.2022.
 */
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        this.repository.clear(userId);
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return this.repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return this.repository.findAll(userId);
        return this.repository.findAll(userId, comparator);
    }

    @NotNull
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return this.repository.findAll(userId);
        return this.repository.findAll(userId, sort);
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.findById(userId, id);
    }

    @Nullable
    @Override
    public M findByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > this.repository.getSize(userId))
            throw new IndexIncorrectException();
        return this.repository.findByIndex(userId, index);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return this.repository.getSize(userId);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public M removeByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > this.repository.getSize(userId))
            throw new IndexIncorrectException();
        return this.repository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M item) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (item == null) return null;
        return this.repository.add(userId, item);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M item) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (item == null) return null;
        return this.repository.remove(userId, item);
    }

}
