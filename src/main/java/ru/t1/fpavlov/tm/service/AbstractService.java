package ru.t1.fpavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.IRepository;
import ru.t1.fpavlov.tm.api.service.IService;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.IndexIncorrectException;
import ru.t1.fpavlov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 23.12.2021.
 */
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    @NotNull AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public M add(@Nullable final M item) {
        if (item == null) return null;
        return this.repository.add(item);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> items) {
        return this.repository.add(items);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> items) {
        return this.repository.set(items);
    }

    public void clear() {
        this.repository.clear();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M item) {
        if (item == null) return null;
        return this.repository.remove(item);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return this.repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable Comparator comparator) {
        if (comparator == null) return this.findAll();
        return this.repository.findAll(comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable Sort sort) {
        if (sort == null) return this.findAll();
        return findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.findById(id);
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IdEmptyException();
        if (index >= this.repository.getSize()) throw new IndexIncorrectException();
        return this.repository.findByIndex(index);
    }

    @Nullable
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        @Nullable final M item = this.findByIndex(index);
        if (item == null) return null;
        return this.repository.removeByIndex(index);
    }

    public boolean isIdExist(@Nullable final String id) {
        return this.findById(id) != null;
    }

    public int getSize() {
        return this.repository.getSize();
    }

}
